package TestingSystem;

public class Question {
    private String content;
    private InfoAboutQuestion infoAboutQuestion;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public InfoAboutQuestion getInfoAboutQuestion() {
        return infoAboutQuestion;
    }

    public Question() {
        this.infoAboutQuestion = new InfoAboutQuestion();
    }
}
