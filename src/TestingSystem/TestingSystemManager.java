package TestingSystem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class TestingSystemManager {
    private ArrayList<Question> questionsArrayList = new ArrayList<>();
    private ArrayList<String> wrongAnsweredQuestions = new ArrayList<>();
    private int questionsCount;
    private int mark;
    private int rightAnswersCount;
    private int falseAnswersCount;

    public static void main(String[] args) {
        TestingSystemManager test1 = new TestingSystemManager();
        test1.test();
    }

    private void test() {
        cleanResults();
        Scanner scan = new Scanner(System.in);
        addQuestions(scan);
        int[] data = startTesting(scan);
        System.out.println(conductTesting(data, scan));
        boolean isAll = false;
        while (!isAll) {
            System.out.println("Что дальше:");
            System.out.println("1: Добавить еще вопросы и пройти тест");
            System.out.println("2: Очистить список вопросов и начать все заново");
            System.out.println("3: Выход");
            System.out.println("4: Заново, не добавляя вопросов");
            int answ = inputAndCheckInteger(scan);
            switch (answ) {
                case 1:
                    addQuestions(scan);
                    StartAgain(scan);
                    break;
                case 2:
                    startWithANewQuestionList(scan);
                    break;
                case 3:
                    isAll = true;
                    break;
                case 4:
                    StartAgain(scan);
            }
        }
    }

    private void cleanResults() {
        mark = 0;
        rightAnswersCount = 0;
        falseAnswersCount = 0;
    }

    private void startWithANewQuestionList(Scanner scan) {
        questionsArrayList.clear();
        questionsCount = 0;
        cleanResults();
        addQuestions(scan);
        int[] data = startTesting(scan);
        System.out.println(conductTesting(data, scan));
    }

    private void StartAgain(Scanner scan) {
        cleanResults();
        int[] data = startTesting(scan);
        System.out.println(conductTesting(data, scan));

    }


    private int inputAndCheckInteger(Scanner scan) {
        boolean isFind = false;
        int digit = 1;
        while (!isFind) {
            if (digit<=0)
                System.out.println("Ввели не то, попробуйте еще раз");
            String str = scan.nextLine();
            try {
                digit = Integer.parseInt(str);
                isFind = true;
            } catch (NumberFormatException exception) {
                System.out.println("Ввели не то, попробуйте еще раз");
                continue;
            }
             if (digit<=0)
                 isFind = false;
        }
        return digit;
    }

    private int inputAndCheckInteger(Scanner scan, String str) {
        boolean isFind = false;
        int digit = 0;
        while (!isFind) {
            try {
                digit = Integer.parseInt(str);
                isFind = true;
            } catch (NumberFormatException exception) {
                System.out.println("Ввели не то, попробуйте еще раз");
                str = scan.nextLine();
                continue;
            }
            if (digit<=0) {
                System.out.println("Ввели не то, попробуйте еще раз");
                isFind = false;
                str = scan.nextLine();
            }
        }
        return digit;
    }

    private int[] startTesting(Scanner scan) {
        boolean isOk = false;
        int[] numbers = new int[questionsCount];
        wrongAnsweredQuestions.clear();
        System.out.println("Сколько вопросов должно попасть в тест?");
        int answersCount = inputAndCheckInteger(scan);
        int[] testQuestionsIndexes = new int[answersCount];
        while (!isOk) {
            if (questionsCount >= answersCount) {
                testQuestionsIndexes = generateRandomizedUniqueIndexes(numbers, answersCount);
                isOk = true;
            } else {
                System.out.println("Ввели недопустимое число, попробуйте еще раз (число вопросов меньше введенного числа)");
                answersCount = inputAndCheckInteger(scan);
            }
        }
        System.out.println("Тестирование началось, ответы писать через пробел (пример 2 3 4)");
        return testQuestionsIndexes;
    }

    private int conductTesting(int[] randomlySortedArrWithNumbers, Scanner scan) {
        double time = 0;
        for (int i = 0; i < randomlySortedArrWithNumbers.length; i++) { //здесь был баг
            int index = randomlySortedArrWithNumbers[i];
            Question question;
            question = questionsArrayList.get(index);
            System.out.println(question.getContent());
            for (int j = 0; j < question.getInfoAboutQuestion().getAnswers().length; j++) {
                System.out.println((j + 1) + ": " + question.getInfoAboutQuestion().getAnswers(j).getContent());
            }
            long startTime = System.currentTimeMillis();
            String userAnswer = scan.nextLine();
            long endTime = System.currentTimeMillis();
            String[] subStr = userAnswer.split(" ");
            ArrayList<Integer> answers = new ArrayList<>();
            for (String str : subStr) {

                int answerNumber = inputAndCheckInteger(scan, str);
                answers.add(answerNumber);
            }
            boolean isContains = isAnsweredCorrectly(answers, question.getInfoAboutQuestion().getRightAnswersIndexes());
            time = time + (endTime - startTime);
            if (isContains) {
                System.out.println("Верно");
                mark = mark + question.getInfoAboutQuestion().getPoints();
                rightAnswersCount++;
            } else {
                wrongAnsweredQuestions.add(question.getContent());
                System.out.println("Неверно");
                falseAnswersCount++;
            }
        }
        System.out.println("вы решали тест: " + time / 1000 + " сек");
        System.out.println("Правильное количество ответов: " + rightAnswersCount);
        System.out.println("Неправильное количество ответов: " + falseAnswersCount);
        for (String str : wrongAnsweredQuestions) {
            System.out.println("Вопрос: " + "'" + str + "'"  + " неверно решили");
        }
        return mark;
    }

    private   int[] generateRandomizedUniqueIndexes(int[] data, int testQuestionsCount) {
        //заполняем массив
        for (int i = 0; i < questionsCount; i++) {
            data[i] = i;
        }
        int[] result = new int[testQuestionsCount]; //код со StackOverFlow (Перемешивает массив и возвращает первые N элементов отсортированного массива)

        int length = data.length;

        Random gen = new Random();

        for (int i = 0; i < testQuestionsCount; i++) {
            int r = gen.nextInt(length);
            result[i] = data[r];
            data[r] = data[length - 1];
            length--;
        }
        return result;
    }

    private   void addQuestions(Scanner scan) {
        System.out.println("Сколько будет вопросов?");
        int addedQuestionCount = inputAndCheckInteger(scan);
        for (int i = 0; i < addedQuestionCount; i++) { //создаются вопросы
            Question question = new Question();
            System.out.println("Введите вопрос");
            String qstn = scan.nextLine();
            question.setContent(qstn);
            System.out.println("Сколько за него баллов");
            int points = inputAndCheckInteger(scan);
            question.getInfoAboutQuestion().setPoints(points);
            int answerCount = inputAnswersCount(scan);
            question = inputAnswers(scan, question, answerCount);//добавляются варианты ответов на вопрос
            questionsArrayList.add(question);
        }
        questionsCount = questionsCount + addedQuestionCount;
    }

    private   int inputAnswersCount(Scanner scan) {
        System.out.println("Введите количество ответов");
        int answersCount = inputAndCheckInteger(scan);
        if (answersCount <= 0) {
            System.out.println("Некорректно, попробуйте еще раз");
            answersCount = inputAnswersCount(scan);
        }
        return answersCount;
    }

    private  Question inputAnswers(Scanner scan, Question question, int answersCount) {
        Answer[] answers = new Answer[answersCount];
        ArrayList<Integer> indexesOfRightAnswers = new ArrayList<>();
        for (int i = 0; i < answersCount; i++) {
            System.out.println("Введите ответ на вопрос");
            Answer answer1 = new Answer();
            String str = scan.nextLine();
            answer1.setContent(str);
            answers[i] = answer1;
            boolean isGetYesOrNo = false;
            while (!isGetYesOrNo) {
                System.out.println("Ответ правильный? (Да/Нет)");
                String trueOrNot = scan.nextLine();
                trueOrNot = trueOrNot.toLowerCase();
                trueOrNot = trueOrNot.replace("-", "");
                switch (trueOrNot) {
                    case ("да"):
                        isGetYesOrNo = true;
                        indexesOfRightAnswers.add(i + 1);
                        break;
                    case ("нет"):
                        isGetYesOrNo = true;
                        break;
                    default:
                        System.out.println("Ввели не то, попробуйте еще раз");
                }
            }
        }
        System.out.println("Ок, на этом все");
        question.getInfoAboutQuestion().setRightAnswersIndex(indexesOfRightAnswers);
        question.getInfoAboutQuestion().setAnswers(answers);
        return question;
    }

    private static boolean isAnsweredCorrectly(ArrayList<Integer> answers, ArrayList<Integer> correctAnswers) {
        int[] answersInt = new int[answers.size()];
        for (int i = 0; i < answers.size(); i++) {
            answersInt[i] = answers.get(i);
        }
        int[] correctAnswersInt = new int[correctAnswers.size()];
        for (int i = 0; i < correctAnswers.size(); i++) {
            correctAnswersInt[i] = correctAnswers.get(i);
        }
        Arrays.sort(answersInt);
        Arrays.sort(correctAnswersInt);
        return Arrays.equals(answersInt, correctAnswersInt);
    }
}
