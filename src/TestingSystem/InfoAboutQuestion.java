package TestingSystem;
import java.util.ArrayList;

public class InfoAboutQuestion {
    private int points;
    private Answer[] answers;
    private ArrayList<Integer> rightAnswersIndex;

    public void setRightAnswersIndex(ArrayList<Integer> rightAnswersIndex) {
        this.rightAnswersIndex = rightAnswersIndex;
    }

    public ArrayList<Integer> getRightAnswersIndexes() {
        return rightAnswersIndex;
    }

    public void setAnswers(Answer[] answers) {
        this.answers = answers;
    }

    public Answer[] getAnswers() {
        return answers;
    }

    public Answer getAnswers(int j) {
        return answers[j];
    }

    public int getPoints() {
        return points;
    }


    public void setPoints(int points) {
        this.points = points;
    }
}
